## debian/tests/makecheck.tst -- GAP Test script
## script format: GAP Reference Manual section 7.9 Test Files (GAP 4r8)
##
gap> TestPackageAvailability( "openmath" , "=11.5.0" , true );
"/usr/share/gap/pkg/openmath"

##
## eos

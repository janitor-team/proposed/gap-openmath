Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0
Upstream-Name: GAP package OpenMath
Upstream-Contact: Alexander Konovalov <alexk@mcs.st-andrews.ac.uk>
Source: https://www.gap-system.org/Packages/openmath.html
X-Source: https://gap-packages.github.io/openmath/
X-Source-Downloaded-From: https://github.com/gap-packages/openmath/releases
X-Upstream-Bugs: https://github.com/gap-packages/openmath/issues
Comment:
 The upstream source tarball is repacked to drop off the regenerated
 material, namely the documentation, to substantially reduce the size.
Files-Excluded:
 doc/chap*.txt
 doc/chap*.html
 doc/chooser.html
 doc/manual.six
 doc/manual.pdf
 doc/lefttoc.css
 doc/manual.js
 doc/manual.css
 doc/nocolorprompt.css
 doc/ragged.css
 doc/rainbow.js
 doc/times.css
 doc/toggless.js
 doc/toggless.css

Files: *
Copyright:
 2007-2020 Alexander Konovalov <alexk@mcs.st-andrews.ac.uk>
 2005-2006 Marco Costantini
 2000-2005 Andrew Solomon
License: GPL-2+

Files: debian/*
Copyright:
 2014-2020 Jerome Benoit <calculus@rezozer.net>
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".
